"use strict";

import { paths } from "../gulpfile.babel";
import gulp from "gulp";
import gulpif from "gulp-if";
import rename from "gulp-rename";
import browsersync from "browser-sync";
import debug from "gulp-debug";
import yargs from "yargs";

const argv = yargs.argv,
  production = !!argv.production;

gulp.task("scripts", () => {
  return gulp
    .src(paths.scripts.src)
    .pipe(
      gulpif(
        production,
        rename({
          suffix: ".min",
        })
      )
    )
    .pipe(gulp.dest(paths.scripts.dist))
    .pipe(
      debug({
        title: "JS files",
      })
    )
    .on("end", browsersync.reload);
});
